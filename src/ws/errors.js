const ERRORS = {
  INVALID_PAYLOAD: 4200,
  TOO_MUCH: 4420,
};

export default ERRORS;
